package com.devcamp.s10.task_5710;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5710Application {

	public static void main(String[] args) {
		SpringApplication.run(Task5710Application.class, args);
	}

}
